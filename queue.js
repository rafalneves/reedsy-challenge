var queue = function(){
	this.data = new Array();
	this.running = false;
};
queue.prototype.push = function(conv){
	this.data.push(conv);
}
queue.prototype.getQueueSize = function(){
	return this.data.length;
}
queue.prototype.getQueueExtimatedTime = function(){
	var time = 0;
	for(var i = 0; i < this.data.length; i++)
		time += this.data[i].to == "html" ? 1 : 5;
	return time;
}
queue.prototype.convLoop = function(){
	if(this.data.length){
		this.running = true;

		var conv = null;

		//check queue for html convs, to give priority to html convesions
		for(var i = 0; i < this.data.length; i++)
			if(this.data[i].to == "html"){
				conv = this.data[i];
				this.data.splice(i, 1);

				break;
			}

		//if non html convs pending take the first pdf in the queue
		if(!conv)
			conv = this.data.shift();

		setTimeout(() => {
			console.log(conv.to + " #" + conv.request);
			this.convLoop();
		}, conv.to == "html" ? 1000 : 5000);
	}else
		this.running = false;
}
module.exports = queue;
