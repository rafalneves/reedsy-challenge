var express = require("express"),
	winston = require("winston"),
	requestMod = require("request");

var Queue = require("./queue.js");

var app = express(),
	queue = new Queue(),
	counter = 0;

queue.convLoop();

app.use(express.static("views"))

app.get("/3", (request, response) => {
	//Run question 3 simulation
	requestMod.post("http://localhost:8080/conversion/pdf");
	requestMod.post("http://localhost:8080/conversion/pdf");
	requestMod.post("http://localhost:8080/conversion/html");
	requestMod.post("http://localhost:8080/conversion/html");
	requestMod.post("http://localhost:8080/conversion/html");
	requestMod.post("http://localhost:8080/conversion/html");
	requestMod.post("http://localhost:8080/conversion/html");
	requestMod.post("http://localhost:8080/conversion/html");
	requestMod.post("http://localhost:8080/conversion/pdf");
	requestMod.post("http://localhost:8080/conversion/html");

	response.end("Check console.");
});
app.get("*", (request, response) => {
	response.writeHead(404, { "Content-Type": "text/plain" });
	response.end();
});

app.post("/conversion/:type", (request, response) => {
	//I'm ignoring request body and original file type

	if(request.params.type == "html"){
		queue.push({ data: "some file data", to: "html", request: counter++ });

		response.writeHead(200, { "Content-Type": "application/json" });
		response.end(new Buffer(JSON.stringify({
			data: {
				event: "Accepted",
				conversionType: "html",
				queueSize: queue.getQueueSize(),
				queueExtimatedTime: queue.getQueueExtimatedTime()
			}
		}), "UTF-8"));
	}else if(request.params.type == "pdf"){
		queue.push({ data: "some file data", to: "pdf", request: counter++ });

		response.writeHead(200, { "Content-Type": "application/json" });
		response.end(new Buffer(JSON.stringify({
			data: {
				event: "Accepted",
				conversionType: "pdf",
				queueSize: queue.getQueueSize(),
				queueExtimatedTime: queue.getQueueExtimatedTime()
			}
		}), "UTF-8"));
	}else{
		response.writeHead(500, { "Content-Type": "application/json" });
		response.end(new Buffer(JSON.stringify({ error: { code: "ConversionTypeNotSupported", msg: "Conversion type not supported" } }), "UTF-8"));
	}

	if(!queue.running)
		queue.convLoop();
});
app.post("*", (request, response) => {
	response.writeHead(404, { "Content-Type": "text/plain" });
	response.end();
});

app.put("*", (request, response) => {
	response.writeHead(404, { "Content-Type": "text/plain" });
	response.end();
});

app.delete("*", (request, response) => {
	response.writeHead(404, { "Content-Type": "text/plain" });
	response.end();
});

app.listen(8080, () => {
	winston.info("Live and listening on 8080");
});
