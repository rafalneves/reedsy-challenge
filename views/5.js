var app = angular.module("app", []);
app.controller("appController", function($scope){
	$scope.operation = new Operation();

	$scope.addOp = function(){
		var op = {};
		op[$scope.f.operation] = $scope.f.data;
		$scope.operation.compose(op);

		$scope.f = {};
	}
	$scope.apply = function(){
		$scope.string = $scope.operation.apply();

		$scope.operation = new Operation();
	}
});

var Operation = function(){
	this.data = new Array();
};
Operation.prototype.compose = function(operation){
	var lastPos = 0;
	for(var i = this.data.length - 1; i >= 0; i--)
		if(this.data[i].move){
			lastPos = this.data[i].move;

			break;
		}

	if(Object.keys(operation)[0] == "move")
		operation.move = lastPos + operation.move;
	this.data.push(operation);
}
Operation.prototype.apply = function(){
	var pos = 0,
		str = '';
	for(var i = 0; i < this.data.length; i++){
		if(Object.keys(this.data[i])[0] == "insert")
			str = str.slice(0, pos) + this.data[i].insert + str.slice(pos);
		else if(Object.keys(this.data[i])[0] == "move")
			pos = Number(this.data[i].move);
		else if(Object.keys(this.data[i])[0] == "delete")
			str = str.slice(0, pos) + str.slice(pos + Number(this.data[i].delete));
	}
	return str;
}
function compose(op1, op2){
	var op = new Operation();
	op.compose(op1);
	op.compose(op2);
	return op;
}
