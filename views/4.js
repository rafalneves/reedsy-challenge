var app = angular.module("app", []);
app.controller("appController", function($scope){
	var dayArr = new Array();
	var	st = 1;
	while(st <= 31){
		dayArr.push(st);
		st++
	}
	$scope.dayArr = dayArr;

	$scope.d = new Date();

	$scope.year = $scope.d.getFullYear();
	$scope.month = String($scope.d.getMonth());
	$scope.day = String($scope.d.getDate());

	$scope.changeDate = function(){
		$scope.d = new Date($scope.year, $scope.month, $scope.day);
	}
});
